from tkinter import *
from configparser import ConfigParser
from tkinter import filedialog, messagebox
import os

parser = ConfigParser()
ini = 'Settings.ini'
parser.read(ini)

if not os.path.isfile(ini):
    parser['app'] = {
        'path': '',
        'app': '',
    }
    parser['app_1'] = {
        'path': '',
        'app': '',
    }
    parser['app_2'] = {
        'path': '',
        'app': '',
    }
    parser['app_3'] = {
        'path': '',
        'app': '',
    }
    parser['app_4'] = {
        'path': '',
        'app': '',
    }
    parser['custom'] = {
        'theme': "Disable",
        'background': "#282828",
        'color': "#f3f3f3",
        'background_hover': "#3f3e38",
        'color_hover': "#f3f3f3"

    }
    with open(ini, 'w') as file:
        parser.write(file)
        file.close()

window = Tk()


class App:
    def __init__(self, main):
        self.main = main
        self.font = ('bold', 13)
        self.enabled_th = parser.get('custom', 'theme')
        self.defaut_th()
        self.main.title('EAR-external-app-runner')
        self.main.resizable(width=False, height=False)

    def defaut_th(self):
        parser.read(ini)
        self.theme_co()
        self.left_bar = Frame(self.main, bg=self.left_bar_bg, highlightcolor=self.left_bar_fg)
        self.left_bar.pack(fill='y',side='left')
        self.app_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, fg=self.left_bar_fg, command=lambda: self.run('app1'), text=os.path.splitext(parser.get('app', 'app', fallback='App1'))[0], bd=0, width=15, height=3)
        self.app_button.grid(row=0, column=0)
        self.app2_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, fg=self.left_bar_fg, command=lambda: self.run('app2'), text=os.path.splitext(parser.get('app_1', 'app', fallback='App2'))[0], bd=0, width=15, height=3)
        self.app2_button.grid(row=1, column=0)
        self.app3_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover,fg=self.left_bar_fg,command=lambda: self.run('app3'), text=os.path.splitext(parser.get('app_2', 'app', fallback='App3'))[0], bd=0, width=15, height=3)
        self.app3_button.grid(row=2, column=0)
        self.app4_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover,fg=self.left_bar_fg,command=lambda: self.run('app4'), text=os.path.splitext(parser.get('app_3', 'app', fallback='App4'))[0], bd=0, width=15, height=3)
        self.app4_button.grid(row=3, column=0)
        self.app5_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, fg=self.left_bar_fg,command=lambda: self.run('app5'), text=os.path.splitext(parser.get('app_4', 'app', fallback='App5'))[0], bd=0, width=15, height=3)
        self.app5_button.grid(row=4, column=0)
        self.right_bar = Frame(self.main, bg=self.right_bar_bg, highlightcolor=self.right_bar_fg)
        self.right_bar.pack(fill='both', side='right', expand=1)
        self.right_bar_add_btn = Button(self.right_bar, bg=self.right_bar_bg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover, text=' Add ', fg=self.right_bar_fg, command=self.add, bd=0, font=self.font, padx=50, pady=15)
        self.right_bar_add_btn.grid(row=0, column=0, padx=3)
        self.right_bar_rem_btn = Button(self.right_bar, bg=self.right_bar_bg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover, text=' Remove ', fg=self.right_bar_fg, command=self.rem_win, bd=0, font=self.font, padx=50, pady=15)
        self.right_bar_rem_btn.grid(row=0, column=1, padx=3)
        self.right_bar_set_btn = Button(self.right_bar, bg=self.right_bar_bg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover, text='Settings', fg=self.right_bar_fg, command=self.app_set, bd=0, font=self.font, padx=134, pady=30)
        self.right_bar_set_btn.grid(row=1, column=0, padx=3, pady=2, columnspan=2)
        self.right_bar_set_ver = Label(self.right_bar, bg=self.right_bar_bg,text='v0.81 - Beta', fg=self.right_bar_fg, font=self.font, padx=114, pady=30)
        self.right_bar_set_ver.grid(row=2, column=0, padx=3, pady=2, columnspan=2)

    def add(self):
        self.add_win = Toplevel(self.main)
        self.add_win.configure(bg=self.right_bar_bg)
        self.add_win.resizable(width=False, height=False)
        self.add_win.title('Add Configuration')
        self.add_win_title = Label(self.add_win, text="Path to App:", activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover,bg=self.right_bar_bg, width=30, fg=self.right_bar_fg, font=self.font)
        self.add_win_title.pack()
        self.add_win_browse = Button(self.add_win, bd=0, text="Browse", activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover, bg=self.right_bar_bg, width=30, fg=self.right_bar_fg, font=self.font, command=self.app_add)
        self.add_win_browse.pack()
        self.add_win_exit = Button(self.add_win,bd=0, text="Exit", activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover, bg=self.right_bar_bg, fg=self.right_bar_fg, width=10, font=self.font, command=self.add_win.destroy)
        self.add_win_exit.pack(padx=3, pady=3, side=RIGHT, expand=1)
        self.add_win.mainloop()

    def theme_co(self):
        parser.read(ini)
        if parser.get('custom', 'theme') == "Enable":
            self.right_bar_bg = parser.get('custom', 'background')
            self.right_bar_fg = parser.get('custom', 'color')
            self.right_bar_bg_hover = parser.get('custom', 'background_hover')
            self.right_bar_fg_hover = parser.get('custom', 'color_hover')
            self.left_bar_bg = parser.get('custom', 'background')
            self.left_bar_fg = parser.get('custom', 'color')
            self.left_bar_bg_hover = parser.get('custom', 'background_hover')
            self.left_bar_fg_hover = parser.get('custom', 'color_hover')
        elif parser.get('custom', 'theme') == "Disable":
            self.right_bar_bg = "#f3f3f3"
            self.right_bar_fg = "#000000"
            self.right_bar_bg_hover = "#777"
            self.right_bar_fg_hover = "#f3f3f3"
            self.left_bar_bg = "#f3f3f3"
            self.left_bar_fg = "#000000"
            self.left_bar_bg_hover = "#777"
            self.left_bar_fg_hover = "#f3f3f3"

    def app_add(self):
        try:
            self.filed = filedialog.askopenfilename(title="add app", filetypes=(("All files", "*.*"),("Any files", "*.*")))
            dirctry = os.path.dirname(self.filed)
            app = os.path.basename(self.filed)
            if parser.get('app', 'app') == "":
                parser.set('app', 'app', app)
                parser.set('app', 'path', dirctry)
            elif parser.get('app', 'app') != "" and parser.get('app_1', 'app') == "":
                parser.set('app_1', 'app', app)
                parser.set('app_1', 'path', dirctry)
            elif parser.get('app', 'app') != "" and parser.get('app_1', 'app') != "" and parser.get('app_2', 'app') == "":
                parser.set('app_2', 'app', app)
                parser.set('app_2', 'path', dirctry)
            elif parser.get('app', 'app') != "" and parser.get('app_1', 'app') != "" and parser.get('app_2', 'app') != "" and parser.get('app_3', 'app') == "":
                parser.set('app_3', 'app', app)
                parser.set('app_3', 'path', dirctry)
            elif parser.get('app', 'app') != "" and parser.get('app_1', 'app') != "" and parser.get('app_2', 'app') != "" and parser.get('app_3', 'app') != "" and parser.get('app_4', 'app') == "":
                parser.set('app_4', 'app', app)
                parser.set('app_4', 'path', dirctry)
            elif parser.get('app', 'app') != "" and parser.get('app_1', 'app') != "" and parser.get('app_2', 'app') != "" and parser.get('app_3', 'app') != "" and parser.get('app_4', 'app') != "":
                messagebox.showinfo('All slots are filled' , 'can add app')

            with open(ini, 'w') as file:
                parser.write(file)

            self.remove()

        except TypeError:
            self.errorx()

    def errorx(self):
        self.err_win = Toplevel(self.main)
        self.err_win_title_error = Label(self.err_win, text='App not added, please select a path', bg=self.left_bar_bg, fg=self.left_bar_fg, padx=30, pady=10)
        self.exit_btn_err = Button(self.err_win, bd=0, text="Close", activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.right_bar_bg, width=30, fg=self.left_bar_fg, font=self.font, command=self.err_win.destroy)
        self.err_win.title('File Dialog Cancelled')
        self.err_win.configure(bg=self.left_bar_bg)
        self.err_win_title_error.pack()
        self.exit_btn_err.pack()
        self.err_win.mainloop()

    def remove(self):
        parser.read(ini)
        self.left_bar.destroy()
        self.app_button.destroy()
        self.app2_button.destroy()
        self.app3_button.destroy()
        self.app4_button.destroy()
        self.app5_button.destroy()
        self.left_bar = Frame(self.main, bg=self.left_bar_bg, highlightcolor=self.left_bar_fg)
        self.left_bar.pack(fill='y', side='left')
        self.app_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, command=lambda: self.run('app1'), fg=self.left_bar_fg, text=os.path.splitext(parser.get('app', 'app', fallback='App1'))[0], bd=0, width=15, height=3)
        self.app_button.grid(row=0, column=0)
        self.app2_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, command=lambda: self.run('app2'), fg=self.left_bar_fg, text=os.path.splitext(parser.get('app_1', 'app', fallback='App2'))[0], bd=0, width=15, height=3)
        self.app2_button.grid(row=1, column=0)
        self.app3_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover,command=lambda: self.run('app3'), fg=self.left_bar_fg, text=os.path.splitext(parser.get('app_2', 'app', fallback='App3'))[0], bd=0, width=15, height=3)
        self.app3_button.grid(row=2, column=0)
        self.app4_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, command=lambda: self.run('app4'), fg=self.left_bar_fg, text=os.path.splitext(parser.get('app_3', 'app', fallback='App4'))[0], bd=0, width=15, height=3)
        self.app4_button.grid(row=3, column=0)
        self.app5_button = Button(self.left_bar, bg=self.left_bar_bg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, command=lambda: self.run('app5'), fg=self.left_bar_fg, text=os.path.splitext(parser.get('app_4', 'app', fallback='App5'))[0], bd=0, width=15, height=3)
        self.app5_button.grid(row=4, column=0)

    def run(self, execution):
        if execution == 'app1':
            if parser.get('app', 'app') != "":
                os.chdir(parser.get('app', 'path'))
                os.system(f"chmod +x ./'{parser.get('app', 'app')}'")
                os.system(f"./'{parser.get('app', 'app')}'")
            elif parser.get('app', 'app') == "":
                pass
        elif execution == 'app2':
            if parser.get('app_1', 'app') != "":
                os.chdir(parser.get('app_1', 'path'))
                os.system(f"chmod +x ./'{parser.get('app_1', 'app')}'")
                os.system(f"./'{parser.get('app_1', 'app')}'")
            elif parser.get('app_1', 'app') == "":
                pass
        elif execution == 'app3':
            if parser.get('app_2', 'app') != "":
                os.chdir(parser.get('app_2', 'path'))
                os.system(f"chmod +x ./'{parser.get('app_2', 'app')}'")
                os.system(f"./'{parser.get('app_2', 'app')}'")
            elif parser.get('app_2', 'app') == "":
                pass
        elif execution == 'app4':
            if parser.get('app_3', 'app') != "":
                os.chdir(parser.get('app_3', 'path'))
                os.system(f"chmod +x ./'{parser.get('app_3', 'app')}'")
                os.system(f"./'{parser.get('app_3', 'app')}'")
            elif parser.get('app_3', 'app') == "":
                pass
        elif execution == 'app5':
            if parser.get('app_4', 'app') != "":
                os.chdir(parser.get('app_4', 'path'))
                os.system(f"chmod +x ./'{parser.get('app_4', 'app')}'")
                os.system(f"./'{parser.get('app_4', 'app')}'")
            elif parser.get('app_4', 'app') == "":
                pass

    def rem_win(self):
        self.win_rem = Toplevel(self.main)
        self.win_rem.configure(bg=self.left_bar_bg)
        self.win_rem.resizable(width=False, height=False)
        self.win_rem.title('Remove Configuration')
        self.app_button_rem = Button(self.win_rem, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.left_bar_bg, fg=self.left_bar_fg, command=lambda: self.remove_app(1), text=os.path.splitext(parser.get('app', 'app', fallback='App1'))[0], bd=0, width=15, height=3)
        self.app_button_rem.grid(row=0, column=0)
        self.app2_button_rem = Button(self.win_rem, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.left_bar_bg, fg=self.left_bar_fg, command=lambda: self.remove_app(2), text=os.path.splitext(parser.get('app_1', 'app', fallback='App2'))[0], bd=0, width=15, height=3)
        self.app2_button_rem.grid(row=1, column=0)
        self.app3_button_rem = Button(self.win_rem, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.left_bar_bg, fg=self.left_bar_fg, command=lambda: self.remove_app(3), text=os.path.splitext(parser.get('app_2', 'app', fallback='App3'))[0], bd=0, width=15, height=3)
        self.app3_button_rem.grid(row=2, column=0)
        self.app4_button_rem = Button(self.win_rem, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.left_bar_bg, fg=self.left_bar_fg, command=lambda: self.remove_app(4), text=os.path.splitext(parser.get('app_3', 'app', fallback='App4'))[0], bd=0, width=15, height=3)
        self.app4_button_rem.grid(row=3, column=0)
        self.app5_button_rem = Button(self.win_rem, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.left_bar_bg, fg=self.left_bar_fg, command=lambda: self.remove_app(5), text=os.path.splitext(parser.get('app_4', 'app', fallback='App5'))[0], bd=0, width=15, height=3)
        self.app5_button_rem.grid(row=4, column=0)
        self.win_rem.mainloop()

    def remove_app(self, removed):
        if removed == 1:
            if parser.get('app', 'app') != "":
                parser.set('app', 'app', '')
                parser.set('app', 'path', '')
            elif parser.get('app', 'app') == "":
                pass
        elif removed == 2:
            if parser.get('app_1', 'app') != "":
                parser.set('app_1', 'app', '')
                parser.set('app_1', 'path', '')
            elif parser.get('app_1', 'app') == "":
                pass
        elif removed == 3:
            if parser.get('app_2', 'app') != "":
                parser.set('app_2', 'app', '')
                parser.set('app_2', 'path', '')
            elif parser.get('app_2', 'app') == "":
                pass
        elif removed == 4:
            if parser.get('app_3', 'app') != "":
                parser.set('app_3', 'app', '')
                parser.set('app_3', 'path', '')
            elif parser.get('app_3', 'app') == "":
                pass
        elif removed == 5:
            if parser.get('app_4', 'app') != "":
                parser.set('app_4', 'app', '')
                parser.set('app_4', 'path', '')
            elif parser.get('app_4', 'app') == "":
                pass

        with open(ini, 'w') as file:
            parser.write(file)
            file.close()

        self.remove()

    def set_text(self):
        parser.read(ini)
        self.win_right_theme_bg_ent.delete(0, END)
        self.win_right_theme_bg_ent.insert(0, parser.get('custom','background'))
        self.win_right_theme_fg_ent.delete(0, END)
        self.win_right_theme_fg_ent.insert(0, parser.get('custom', 'color'))
        self.win_right_theme_bg_hover_ent.delete(0, END)
        self.win_right_theme_bg_hover_ent.insert(0, parser.get('custom','background_hover'))
        self.win_right_theme_fg_hover_ent.delete(0, END)
        self.win_right_theme_fg_hover_ent.insert(0, parser.get('custom', 'color_hover'))

    def app_set(self):
        self.win_set = Toplevel(self.main)
        self.win_set.title('Settings')
        self.win_set.resizable(width=False, height=False)
        self.win_set.configure(bg=self.left_bar_bg)
        self.win_theme_title = Label(self.win_set,text='Custom-theme:', bg=self.left_bar_bg, fg=self.left_bar_fg)
        self.win_theme_title.grid(row=0, column=0,columnspan=5)
        self.win_right_theme_title = Label(self.win_set,text='Element:', bg=self.left_bar_bg, fg=self.left_bar_fg,font=('bold', 11))
        self.win_right_theme_title.grid(row=1, column=0,columnspan=3)
        self.win_right_theme_bg_title = Label(self.win_set, text='Background:', bg=self.left_bar_bg, fg=self.left_bar_fg)
        self.win_right_theme_bg_title.grid(row=2,column=0, pady=3)
        self.win_right_theme_bg_ent = Entry(self.win_set, bg=self.left_bar_bg, fg=self.left_bar_fg, bd=0)
        self.win_right_theme_bg_ent.grid(row=2,column=1,pady=3)
        self.win_right_theme_fg_title = Label(self.win_set, text='Color:', bg=self.left_bar_bg, fg=self.left_bar_fg)
        self.win_right_theme_fg_title.grid(row=3, column=0,pady=3)
        self.win_right_theme_fg_ent = Entry(self.win_set, bg=self.left_bar_bg, fg=self.left_bar_fg,bd=0)
        self.win_right_theme_fg_ent.grid(row=3, column=1, pady=3)
        self.win_right_theme_fg_hover_title = Label(self.win_set, text='Color_hover:', bg=self.left_bar_bg, fg=self.left_bar_fg)
        self.win_right_theme_fg_hover_title.grid(row=4, column=0,pady=3)
        self.win_right_theme_fg_hover_ent = Entry(self.win_set, bg=self.left_bar_bg, fg=self.left_bar_fg,bd=0)
        self.win_right_theme_fg_hover_ent.grid(row=4, column=1, pady=3)
        self.win_right_theme_bg_hover_title = Label(self.win_set, text='Background_hover:', bg=self.left_bar_bg, fg=self.left_bar_fg)
        self.win_right_theme_bg_hover_title.grid(row=5, column=0, pady=3)
        self.win_right_theme_bg_hover_ent = Entry(self.win_set, bg=self.left_bar_bg, fg=self.left_bar_fg, bd=0)
        self.win_right_theme_bg_hover_ent.grid(row=5, column=1, pady=3)
        self.set_text()
        self.win_inp_theme = Button(self.win_set, bd=0, text="Enable", command=self.set_theme,activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.right_bar_bg, width=15, fg=self.left_bar_fg, font=self.font)
        self.win_inp_theme.grid(row=6,column=0,columnspan=1)
        self.win_theme_add = Button(self.win_set, bd=0, text="Set Custom theme", command=self.set_custom, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover, bg=self.right_bar_bg, fg=self.left_bar_fg, font=self.font)
        self.win_theme_add.grid(row=6,column=1)
        self.win_set.mainloop()

    def set_custom(self):
        parser.read(ini)
        parser.set('custom', 'background', self.win_right_theme_bg_ent.get())
        parser.set('custom', 'color', self.win_right_theme_fg_ent.get())
        parser.set('custom', 'background_hover', self.win_right_theme_bg_hover_ent.get())
        parser.set('custom', 'color_hover', self.win_right_theme_fg_hover_ent.get())
        with open(ini, 'w') as file:
            parser.write(file)
            file.close()
            self.set_text()

    def set_theme(self):
        parser.read(ini)
        if parser.get('custom', 'theme') == 'Enable':
            self.right_bar_bg = "#f3f3f3"
            self.right_bar_fg = "#000000"
            self.right_bar_bg_hover = "#777"
            self.right_bar_fg_hover = "#f3f3f3"
            self.left_bar_bg = "#f3f3f3"
            self.left_bar_fg = "#000000"
            self.left_bar_bg_hover = "#777"
            self.left_bar_fg_hover = "#f3f3f3"
            # settings
            self.win_set.configure(bg=self.left_bar_bg)
            self.win_theme_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_bg_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_bg_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_right_theme_bg_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_right_theme_bg_hover_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_hover_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_bg_hover_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_hover_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_right_theme_bg_hover_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_inp_theme.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activeforeground=self.left_bar_fg_hover, activebackground=self.left_bar_bg_hover)
            self.win_theme_add.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activeforeground=self.left_bar_fg_hover, activebackground=self.left_bar_bg_hover)
            # left bar
            self.left_bar.configure(bg=self.left_bar_bg)
            self.app_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app2_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app3_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app4_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app5_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            # right bar
            self.right_bar.configure(bg=self.right_bar_bg)
            self.right_bar_add_btn.configure(bg=self.right_bar_bg, fg=self.right_bar_fg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover)
            self.right_bar_set_btn.configure(bg=self.right_bar_bg, fg=self.right_bar_fg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover)
            self.right_bar_rem_btn.configure(bg=self.right_bar_bg, fg=self.right_bar_fg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover)
            self.right_bar_set_ver.configure(bg=self.right_bar_bg,fg=self.right_bar_fg)
            parser.set('custom', 'theme', 'Disable')
        elif parser.get('custom', 'theme') == 'Disable':
            self.right_bar_bg = parser.get('custom', 'background')
            self.right_bar_fg = parser.get('custom', 'color')
            self.right_bar_bg_hover = parser.get('custom', 'background_hover')
            self.right_bar_fg_hover = parser.get('custom', 'color_hover')
            self.left_bar_bg = parser.get('custom', 'background')
            self.left_bar_fg = parser.get('custom', 'color')
            self.left_bar_bg_hover = parser.get('custom', 'background_hover')
            self.left_bar_fg_hover = parser.get('custom', 'color_hover')
            # settings
            self.win_set.configure(bg=self.left_bar_bg)
            self.win_theme_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_bg_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_bg_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_right_theme_bg_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_right_theme_bg_hover_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_hover_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_bg_hover_title.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.win_right_theme_fg_hover_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_right_theme_bg_hover_ent.configure(bg=self.left_bar_bg, fg=self.left_bar_fg)
            self.win_inp_theme.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activeforeground=self.left_bar_fg_hover, activebackground=self.left_bar_bg_hover)
            self.win_theme_add.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activeforeground=self.left_bar_fg_hover, activebackground=self.left_bar_bg_hover)

            # left bar
            self.left_bar.configure(bg=self.left_bar_bg)
            self.app_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app2_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app3_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app4_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            self.app5_button.configure(bg=self.left_bar_bg, fg=self.left_bar_fg, activebackground=self.left_bar_bg_hover, activeforeground=self.left_bar_fg_hover)
            # right bar
            self.right_bar.configure(bg=self.right_bar_bg)
            self.right_bar_add_btn.configure(bg=self.right_bar_bg, fg=self.right_bar_fg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover)
            self.right_bar_set_btn.configure(bg=self.right_bar_bg, fg=self.right_bar_fg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover)
            self.right_bar_rem_btn.configure(bg=self.right_bar_bg, fg=self.right_bar_fg, activebackground=self.right_bar_bg_hover, activeforeground=self.right_bar_fg_hover)
            self.right_bar_set_ver.configure(bg=self.right_bar_bg,fg=self.right_bar_fg)
            parser.set('custom', 'theme', 'Enable')

        with open(ini, 'w') as file:
            parser.write(file)
            file.close()


exe = App(window)
window.mainloop()
